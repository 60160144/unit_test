
import com.mycompany.test.Player;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import com.mycompany.test.Table;

public class TableTest {
    
    public TableTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    public void testR1Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,1);
        table.setRowCol(1,2);
        table.setRowCol(1,3);
        assertEquals(true,table.checkWin());
    }
    
     public void testR2Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(2,1);
        table.setRowCol(2,2);
        table.setRowCol(2,3);
        assertEquals(true,table.checkWin());
    }
     
     public void testR3Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(3,1);
        table.setRowCol(3,2);
        table.setRowCol(3,3);
        assertEquals(true,table.checkWin());
    }
     
     public void testC1Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,1);
        table.setRowCol(2,1);
        table.setRowCol(3,1);
        assertEquals(true,table.checkWin());
    }
     
      public void testC2Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,2);
        table.setRowCol(2,2);
        table.setRowCol(3,2);
        assertEquals(true,table.checkWin());
    }
      
      public void testC3Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,3);
        table.setRowCol(2,3);
        table.setRowCol(3,3);
        assertEquals(true,table.checkWin());
    }
      
      public void testa1Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,1);
        table.setRowCol(2,2);
        table.setRowCol(3,3);
        assertEquals(true,table.checkWin());
    }
      
       public void testa2Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,3);
        table.setRowCol(2,2);
        table.setRowCol(3,1);
        assertEquals(true,table.checkWin());
    }
       
       public void testSwitchPlayer(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.switchPlayer();
        assertEquals('x',table.getCurrentPlayer().getName());
       }
       
       public void testa2Lose(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,3);
        table.setRowCol(2,2);
        table.setRowCol(3,2);
        assertEquals(false,table.checkWin());
       }
}
